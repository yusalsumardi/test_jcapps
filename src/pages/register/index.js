import { View, Text, ScrollView, TextInput, StyleSheet, Alert } from 'react-native';
import React, { Component } from 'react';

class index extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View>
                <ScrollView>
                    <View>
                        <Text style={styles.titleText} >First Name*</Text>
                        <TextInput placeholder={"Masukan Nama Anda"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View>
                        <Text style={styles.titleText}>Last Name</Text>
                        <TextInput placeholder={"Masukan Nama Anda"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View>
                        <Text style={styles.titleText}>Email*</Text>
                        <TextInput placeholder={"admin@gmail.com"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View>
                        <Text style={styles.titleText}>Phone Number*</Text>
                        <TextInput placeholder={"08120012345"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View>
                        <Text style={styles.titleText}>Birthdate</Text>
                        <TextInput placeholder={"Pilih mm/dd/yyyy"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View>
                        <Text style={styles.titleText}>Gender</Text>
                        <TextInput placeholder={"Pilih male or female"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View>
                        <Text style={styles.titleText}>Password*</Text>
                        <TextInput placeholder={"Masukkan password anda"} maxLength={50} style={styles.inputText} />
                    </View>
                    <View style={styles.buttonSubmit}>
                        <Text style={styles.buttonSubmitText} onPress={() => Alert.alert('Registered')}>
                            submit
                        </Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputText: {
        borderWidth: 1,
        backgroundColor: "#e6e6e6",
        height: 35,
        marginHorizontal: 20,
        marginTop: 5,
        paddingLeft: 10,
    },
    titleText: {
        fontSize: 20,
        paddingLeft: 30,
        marginTop: 10,
    },
    buttonSubmitText: {
        fontSize: 20,
    },
    buttonSubmit: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        borderWidth: 1,
        backgroundColor: "#e6e6e6",
        borderRadius: 5,
        marginHorizontal: 150,
        height: 40,
    }
});

export default index;